package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-kivik/kivik/v4"
	"interact_CouchDB/common/config"
	"io/ioutil"
	"net/http"
)

// Define data structure
type Response struct {
	Record []struct {
		Studentid      int         `json:"studentid"`
		Studentname    string      `json:"studentname"`
		Gender         string      `json:"gender"`
		Info           interface{} `json:"info"`
		IsClassMonitor bool        `json:"is_class_monitor"`
		Classname      string      `json:"classname"`
		Teamname       string      `json:"teamname"`
	} `json:"record"`
}

type RecordStudent struct {
	Studentid      int         `json:"studentid"`
	Rev            string      `json:"_rev,omitempty"`
	Studentname    string      `json:"studentname"`
	Gender         string      `json:"gender"`
	Info           interface{} `json:"info"`
	IsClassMonitor bool        `json:"is_class_monitor"`
	Classname      string      `json:"classname"`
	Teamname       string      `json:"teamname"`
}

// Post data to coucdb
func CreateStudent(ctx *gin.Context) {
	// json data
	url := "https://api.jsonbin.io/v3/b/63155201a1610e63861e772b"
	res, err := http.Get(url)
	if err != nil {
		panic(err.Error())
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err.Error())
	}
	var data Response
	// unmarshall
	json.Unmarshal(body, &data)
	// print values of the object
	fmt.Printf(string(body))
	stdlist := make([]interface{}, len(data.Record))
	for i, v := range data.Record {
		stdlist[i] = v
	}
	config.Db.BulkDocs(context.TODO(), stdlist)
	ctx.JSONP(http.StatusOK, "You have successfully created document.")
}

// Get by ID
func GetStudentById(ctx *gin.Context) {
	row := config.Db.Get(context.TODO(), ctx.Param("id"))
	var student RecordStudent
	if err := row.ScanDoc(&student); err != nil {
		ctx.JSON(500, err.Error())
	}
	ctx.JSON(http.StatusOK, student)
}

// Get by rev
func GetStudentByRev(ctx *gin.Context) {
	row := ctx.Param("rev")
	student := config.Db.Query(context.TODO(), "_design/getbyrev", "_view/viewbyrev", kivik.Options{
		"key":          row,
		"include_docs": true,
	})
	var stu RecordStudent
	if err := student.ScanDoc(&stu); err != nil {
		panic(err)
	}
	ctx.JSON(http.StatusOK, stu)
}

// Update by id and rev
func UpdateStudentById(ctx *gin.Context) {
	var user RecordStudent      // Create object user
	user.Rev = ctx.Param("rev") // Must be set

	ctx.BindJSON(&user) // Convert object to json (body) for request
	updateUser, err := config.Db.Put(context.TODO(), ctx.Param("id"), user)

	if err != nil {
		panic(err)
	}
	ctx.JSONP(http.StatusOK, updateUser)
}

// delete by Id and Rev
func DeleteStudentById(ctx *gin.Context) {
	var user RecordStudent

	ctx.BindJSON(&user)
	deleteUser, err := config.Db.Delete(context.TODO(), ctx.Param("id"), ctx.Param("rev"))

	if err != nil {
		panic(err)
	}

	ctx.JSONP(http.StatusOK, deleteUser)
}

// design document with map reduce for filter classname
func FilterByClassname(ctx *gin.Context) {
	row := ctx.Param("classname")
	student := config.Db.Query(context.TODO(), "_design/filtername", "_view/filter_Classname",
		map[string]interface{}{
			"key":          row,
			"reduce":       false,
			"include_docs": true,
		})
	var std RecordStudent
	var list []interface{}
	for student.Next() {
		if err := student.ScanDoc(&std); err != nil {
			panic(err)
		}
		list = append(list, std)
	}
	ctx.JSONP(http.StatusOK, list)
}
