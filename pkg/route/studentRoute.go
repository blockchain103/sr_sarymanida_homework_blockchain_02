package route

import (
	"github.com/gin-gonic/gin"
	"interact_CouchDB/pkg/repository"
)

func RigisterRoutes(r *gin.Engine) {
	router := r.Group("api/v1/student/") //group of request endpoint
	router.POST("create", repository.CreateStudent)
	router.GET("get_by_id/:id", repository.GetStudentById)
	router.GET("get_by_rev/:rev", repository.GetStudentByRev)
	router.PUT("update_by_id/:id/:rev", repository.UpdateStudentById)
	router.DELETE("delete_by_id/:id/:rev", repository.DeleteStudentById)
	router.GET("filter_by_classname/:classname", repository.FilterByClassname)
}
