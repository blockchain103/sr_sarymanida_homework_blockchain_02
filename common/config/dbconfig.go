package config

import (
	"context"
	_ "github.com/gin-gonic/gin"
	_ "github.com/go-kivik/couchdb/v4" // The CouchDB driver
	"github.com/go-kivik/kivik/v4"
)

var Db *kivik.DB

func Config() {
	dsn := "http://admin:1234@34.173.112.173:5984/"
	client, err := kivik.New("couch", dsn)
	if err != nil {
		panic(err)
	}
	client.CreateDB(context.TODO(), "students")
	db := client.DB("students")
	Db = db

}
