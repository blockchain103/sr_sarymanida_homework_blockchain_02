# SR_Sarymanida_homework_blockchain_02



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/blockchain103/sr_sarymanida_homework_blockchain_02.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/blockchain103/sr_sarymanida_homework_blockchain_02/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

# Why do we need to have a REST API for an existing API of CouchDB?

- endpoint ងាយស្រួលប្រើនៅពេលក្រោយដោយមិនចាំបាច់សរសេរ​ curl ម្តងទៀត ។
- endpoint អាចអោយនិយមន័យបានច្បាស់លាស់ទៅកាន់ developer ។
Example 
តាម curl : មិនច្បាស់លាស់​ថា delete អ្វី
  curl -X DELETE http://username:password@ip:port/database/id_document?rev 
តាម endpoint : បញ្ចាក់ថា delete student​​ ដោយ id (student/delete_by_id)
  localhost:8080/api/v1/student/delete_by_id/id/rev

- Endpoint អាចធ្វើអោយ developer ងាយស្រួលយល់ ជាង API នៅក្នុង couchdb ពីព្រោះនៅក្នុង couchdb ប្រើ curl ហើយមានភាពស្មុកស្មាញ និង​ ប្រើពេលយូរ ។ 
- ងាយស្រួលមើល data ពីព្រោះវាមានតម្រង់ជា JSON 
- វាមានភាព security ជាងពីព្រោនៅក្នុង endpoint នីមួយៗ មិនត្រូវការ add user និង​ password ដូចប្រើ curl នៅក្នុង couchdb ទេ ។
- REST API ផ្តល់នូវវិធីក្នុងការ access web services ដែលមានលក្ខណះ flexible ដោយមិនចាំបាច់ត្រូវការ massive processing capabilities ។
- REST API មាន security ពីព្រោះវវា support ជាមួយ​  Transport Layer Security (TLS) encryption ដែលវាជួយអោយ internet connection របស់យើងមានលក្ខណះ private និង​វាជួយ check មើល data ដែលបាន​ send រវៀង server និង client ថាត្រូវបាន encrypted និង​ មិនអាច​ modified បាន ។
- REST API ប្រើប្រាស់ Hypertext Transfer Protocol (HTTP) សម្រាប់ requests ដើម្បី create, read, update, និង delete data បាន​ ។ ជាពិសេសអាច handle បាន​ គ្រប់ type នៃ data ។



# Credentails 
http://34.173.112.173:5984/_utils/#login
username : admin
password : 1234

# Database name
 "Student"

# Post data to couchdb 
localhost:8080/api/v1/student/create

# Get by Id
localhost:8080/api/v1/student/get_by_id/27c80078a7e3b330142ba56f9f01c53d

# Get by Rev 
localhost:8080/api/v1/student/get_by_rev/1-a3025892b9fac42b741c8ec869cc73df

# Delete
localhost:8080/api/v1/student/delete_by_id/27c80078a7e3b330142ba56f9f024c4f/1-90608f31a2359bdadd340ac04e4ccbd2

# Update 
localhost:8080/api/v1/student/update_by_id/27c80078a7e3b330142ba56f9f01bc67/2-574a2aa28963b0703151b64dbe0c5b4d

{
  "studentid": 1,
  "studentname": "Ronaldo",
  "gender": "Male",
  "info": null,
  "is_class_monitor": false,
  "classname": "SR",
  "teamname": "Blockchain"
}

# Map/reduce by filter classname
localhost:8080/api/v1/student/filter_by_classname/SR




