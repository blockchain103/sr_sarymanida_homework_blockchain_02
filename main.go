package main

import (
	"github.com/gin-gonic/gin"
	_ "github.com/gin-gonic/gin"
	_ "github.com/go-kivik/couchdb/v4" // The CouchDB driver
	"interact_CouchDB/common/config"
	"interact_CouchDB/pkg/route"
)

var router *gin.Engine

func main() {
	router := gin.Default()
	router.SetTrustedProxies([]string{"34.173.112.173"})
	config.Config()
	route.RigisterRoutes(router)

	router.Run(":8080")

}
